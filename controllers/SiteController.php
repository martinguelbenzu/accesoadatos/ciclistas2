<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Ciclista;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    //
    //
    //CONSULTAS 
    //
    //Consulta 1a
    public function actionConsulta1a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("COUNT(*) num_ciclistas")->distinct(),
            'pagination'=>[
                    'pageSize'=>0,
            ]
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['num_ciclistas'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay.",
            "sql"=>"SELECT DISTINCT COUNT(*) AS Num_Ciclistas FROM ciclista;",
           ]);
    }
    //Consulta 1
    public function actionConsulta1(){
        $numero = Yii::$app->db
            ->createCommand('SELECT 1')
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>'SELECT DISTINCT COUNT(dorsal) num_ciclistas FROM ciclista;',

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 0,
            ]
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['num_ciclistas'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Número de ciclistas que hay.",
            "sql"=>"SELECT DISTINCT COUNT(*) AS num_ciclistas FROM ciclista;",
           ]);
    }
    
    //Consulta 2a
    public function actionConsulta2a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("count(*) num_ciclistas")->distinct()->where("nomequipo='Banesto'"),
            'pagination'=>[
                    'pageSize'=>10,
            ]
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['num_ciclistas'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto.",
            "sql"=>"SELECT DISTINCT COUNT(*) AS num_ciclistas FROM ciclista WHERE nomequipo='Banesto';",
        ]);
    }
    //Consulta 2
    public function actionConsulta2(){
        $numero = Yii::$app->db
            ->createCommand("SELECT 1")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT COUNT(*) AS num_ciclistas FROM ciclista WHERE nomequipo='Banesto';",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['num_ciclistas'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto.",
            "sql"=>"SELECT DISTINCT COUNT(*) AS num_ciclistas FROM ciclista WHERE nomequipo='Banesto';",
        ]);
    }
    
    //Consulta 3a
    public function actionConsulta3a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select ("AVG(edad) as avg_age")->distinct(),
            'pagination'=>[
                    'pageSize'=>10,
                ]
            ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['avg_age'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas.",
            "sql"=>"SELECT DISTINCT AVG(edad) AS edad_media FROM ciclista;",
        ]);
    }
    //Consulta 3
    public function actionConsulta3(){
        $numero = Yii::$app->db
            ->createCommand("SELECT 1")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT AVG(edad) avg_age FROM ciclista;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['avg_age'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas.",
            "sql"=>"SELECT DISTINCT AVG(edad) AS edad_media FROM ciclista;",
        ]);
    }
    
    //Consulta 4a
    public function actionConsulta4a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select ("nomequipo,AVG(edad) as avg_age")->distinct()->where("nomequipo='Banesto'"),
            'pagination'=>[
                    'pageSize'=>10,
                ]
            ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo','avg_age'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"La edad media de los del equipo Banesto.",
            "sql"=>"SELECT DISTINCT nomequipo, AVG(edad) AS Edad_Media_Banesto FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    //Consulta 4
    public function actionConsulta4(){
        $numero = Yii::$app->db
            ->createCommand('SELECT 1')
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT nomequipo, AVG(edad) AS avg_age FROM ciclista WHERE nomequipo='Banesto';",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo','avg_age'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto.",
            "sql"=>"SELECT DISTINCT nomequipo, AVG(edad) AS Edad_Media_Banesto FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    
    //Consulta 5a
    public function actionConsulta5a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select ("nomequipo , AVG(edad) avg_age")->distinct()->groupBy("nomequipo"),
            'pagination'=>[
                    'pageSize'=>25,
                ]
            ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo','avg_age'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo.",
            "sql"=>"SELECT DISTINCT nomequipo, AVG(edad) AS Edad_Media FROM ciclista GROUP BY nomequipo;",
        ]);
    }
    //Consulta 5
    public function actionConsulta5(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(DISTINCT nomequipo) FROM ciclista")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT nomequipo, AVG(edad) avg_age FROM ciclista GROUP BY nomequipo;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 0,
            ]
        ]);
        return $this->render("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo','avg_age'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo.",
            "sql"=>"SELECT DISTINCT nomequipo, AVG(edad) AS Edad_Media FROM ciclista GROUP BY nomequipo;",
        ]);
    }
    
    //Consulta 6a
    public function actionConsulta6a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select ("nomequipo , count(dorsal) num_ciclistas")->distinct()->groupBy("nomequipo"),
            'pagination'=>[
                    'pageSize'=>0,
                ]
            ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo','num_ciclistas'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo.",
            "sql"=>"SELECT DISTINCT nomequipo, COUNT(*) AS Num_Ciclistas FROM ciclista GROUP BY nomequipo;",
        ]);
    }
    //Consulta 6
    public function actionConsulta6(){
        $numero = Yii::$app->db
            ->createCommand("SELECT DISTINCT nomequipo, COUNT(*) AS num_ciclistas FROM ciclista GROUP BY nomequipo;")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT nomequipo, COUNT(*) AS num_ciclistas FROM ciclista GROUP BY nomequipo;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo','num_ciclistas'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"El número de ciclistas por equipo.",
            "sql"=>"SELECT DISTINCT nomequipo, COUNT(*) AS Num_Ciclistas FROM ciclista GROUP BY nomequipo;",
        ]);
    }
    
    //Consulta 7a
    public function actionConsulta7a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Puerto::find() ->select ("COUNT(*) num_puertos"),
            'pagination'=>[
                    'pageSize'=>0,
                ]
            ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>[ "num_puertos"],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número total de puertos.",
            "sql"=>"SELECT DISTINCT COUNT(*) AS Num_Puertos FROM puerto;",
        ]);
    }
    //Consulta 7
    public function actionConsulta7(){
        $numero = Yii::$app->db
            ->createCommand("SELECT 1")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT COUNT(*) AS num_puertos FROM puerto;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 0,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>[ "num_puertos"],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos.",
            "sql"=>"SELECT DISTINCT COUNT(*) AS Num_Puertos FROM puerto;",
        ]);
    }
    
    //Consulta 8a
    public function actionConsulta8a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Puerto::find() ->select ("COUNT(*) num_puertos")->distinct()->where("altura>1500"),
            'pagination'=>[
                    'pageSize'=>0,
                ]
            ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['num_puertos'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El número total de puertos mayores de 1500.",
            "sql"=>"SELECT DISTINCT COUNT(*) AS num_puertos FROM puerto WHERE altura>1500;",
        ]);
    }
    //Consulta 8
    public function actionConsulta8(){
        $numero = Yii::$app->db
            ->createCommand("SELECT 1")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT COUNT(*) AS num_puertos FROM puerto WHERE altura>1500;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 0,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['num_puertos'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500.",
            "sql"=>"SELECT DISTINCT COUNT(*) AS num_puertos FROM puerto WHERE altura>1500;",
        ]);
    }
    
    //Consulta 9a
    public function actionConsulta9a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select ("nomequipo, count(*) num_ciclistas")->distinct()->groupBy("nomequipo")->having("count(*)>4"),
            'pagination'=>[
                    'pageSize'=>10,
                ]
            ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo','num_ciclistas'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas.",
            "sql"=>"SELECT nomequipo, count(*) num_ciclistas FROM ciclista GROUP BY nomequipo HAVING count(*)>4;",
        ]);
    }
    //Consulta 9
    public function actionConsulta9(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(nomequipo) FROM ciclista GROUP BY nomequipo HAVING count(*)>4")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT nomequipo, count(*) num_ciclistas FROM ciclista GROUP BY nomequipo HAVING count(*)>4;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 10,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo','num_ciclistas'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas.",
            "sql"=>"SELECT nomequipo, count(*) num_ciclistas FROM ciclista GROUP BY nomequipo HAVING count(*)>4;",
        ]);
    }
    
    //Consulta 10a
    public function actionConsulta10a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()   ->select ("nomequipo, count(*) num_ciclistas")->distinct()
                                            ->groupBy("nomequipo") ->where("edad BETWEEN 28 AND 32")
                                                ->having("count(*)>4"),
            'pagination'=>[
                    'pageSize'=>10,
                ]
            ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo','num_ciclistas'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32.",
            "sql"=>"SELECT nomequipo, count(*) num_ciclistas FROM ciclista GROUP BY nomequipo HAVING count(*)>4 AND edad BETWEEN 28 AND 32;",
        ]);
    }
    //Consulta 10
    public function actionConsulta10(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(nomequipo) FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING count(*)>4")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT nomequipo, count(*) num_ciclistas FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING count(*)>4;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 0,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo','num_ciclistas'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32.",
            "sql"=>"SELECT nomequipo, count(*) num_ciclistas "
                    . "FROM ciclista WHERE edad BETWEEN 28 AND 32 "
                        . "GROUP BY nomequipo HAVING count(*)>4;",
        ]);
    }
    
    //Consulta 11a
    public function actionConsulta11a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Etapa::find() ->select ("dorsal,count(*) num_etapas")->distinct()->groupBy("dorsal"),
            'pagination'=>[
                    'pageSize'=>8,
                ]
            ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal','num_etapas'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas.",
            "sql"=>"SELECT DISTINCT dorsal, COUNT(*) num_etapas FROM etapa GROUP BY dorsal;",
        ]);
    }
    //Consulta 11
    public function actionConsulta11(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(DISTINCT dorsal) FROM etapa GROUP BY dorsal")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT DISTINCT dorsal, COUNT(*) num_etapas FROM etapa GROUP BY dorsal;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 8,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal','num_etapas'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas.",
            "sql"=>"SELECT DISTINCT dorsal, COUNT(*) num_etapas FROM etapa GROUP BY dorsal;",
        ]);
    }
    
    //Consulta 12a
    public function actionConsulta12a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Etapa::find() ->select ("dorsal,count(*) num_etapas")->distinct()->groupBy("dorsal")->having("num_etapas>1"),
            'pagination'=>[
                    'pageSize'=>8,
                ]
            ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal','num_etapas'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa.",
            "sql"=>"SELECT DISTINCT dorsal,COUNT(*) num_etapas FROM etapa GROUP BY dorsal HAVING num_etapas>1;",
        ]);
    }
    //Consulta 12
    public function actionConsulta12(){
        $numero = Yii::$app->db
            ->createCommand("SELECT count(DISTINCT dorsal),COUNT(*) num_etapas FROM etapa GROUP BY dorsal HAVING num_etapas>1")
            ->queryScalar();
        $dataProvider = new SqlDataProvider([

            'sql'=>"SELECT dorsal,COUNT(*) num_etapas FROM etapa GROUP BY dorsal HAVING num_etapas>1;",

            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 8,
            ]
        ]);
        return $this->render ("resultado",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal','num_etapas'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa.",
            "sql"=>"SELECT DISTINCT dorsal,COUNT(*) num_etapas FROM etapa GROUP BY dorsal HAVING num_etapas>1;",
        ]);
    }
}
