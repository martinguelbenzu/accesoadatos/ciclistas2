<?php

use yii\grid\GridView;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="row">
    <div class="col-lg-12">
        <div class="thumbnail">
            <div class="caption">
                <h1>Consulta</h1>
                <h2><?=$titulo?></h2>
                <p class="lead"><?=$enunciado?></p>
                <p class="well"><?=$sql?></p>
            </div>
        </div>
    </div>
</div>
<?=GridView::widget([
    'dataProvider' => $resultado,
    'columns'=>$campos
    ]); 
?>
